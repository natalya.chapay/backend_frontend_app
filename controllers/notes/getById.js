const { createError } = require('../../helpers');

const { Note } = require('../../models/note');

const getById = async (req, res) => {
  const { id } = req.params;
  const result = await Note.findById(id);
  if (!result) {
    throw createError(404);
  }
  res.json({
    note: result,
  });
};

module.exports = getById;
