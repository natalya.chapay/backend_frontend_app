const express = require('express');

const users = require('../../controllers/users');

const { ctrlWrapper } = require('../../helpers');
const { user } = require('../../middlewares');

const router = express.Router();

router.get('/me', user, ctrlWrapper(users.getUser));

router.delete('/me', user, ctrlWrapper(users.deleteUser));

router.patch('/me', user, ctrlWrapper(users.updatePassword));

module.exports = router;
