const { createError } = require('../../helpers');

const { Note } = require('../../models/note');

const updateNote = async (req, res) => {
  const { id } = req.params;
  const text = req.body;
  const result = await Note.findByIdAndUpdate(id, text, { new: true });
  if (!result) {
    throw createError(404);
  }
  res.status(200).json({
    message: 'Success',
  });
};

module.exports = updateNote;
