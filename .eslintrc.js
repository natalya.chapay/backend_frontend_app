module.exports = {
  env: {
    browser: true,
	  es6: true,
    commonjs: true,
    es2021: true,
    node: true    
  },
  extends: ['standard', 'prettier'],
  parserOptions: {
    ecmaVersion: 12,
  },
  rules: {
  },
}