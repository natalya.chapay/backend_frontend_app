const notesRouter = require('./notes');
const usersRouter = require('./users');
const authRouter = require('./auth');

module.exports = {
  notesRouter,
  usersRouter,
  authRouter,
};
