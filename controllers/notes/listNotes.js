const { Note } = require('../../models/note');

const listNotes = async (req, res) => {
  const { _id } = req.user;
  const { page = 1, limit = 0 } = req.query;
  const offset = (page - 1) * limit;
  const query = { userId: _id };
  const count = await Note.count(query);
  const result = await Note.find(query, '', { skip: Number(offset), limit: Number(limit) }).populate('userId', '_id username');

  res.json({
    offset,
    limit,
    count,
    notes: result,
  });
};

module.exports = listNotes;
