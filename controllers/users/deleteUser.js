const { User } = require('../../models');

const deleteUser = async (req, res) => {
  const { user } = req;

  await User.deleteOne(user);

  res.json({
    message: 'Success',
  });
};

module.exports = deleteUser;
