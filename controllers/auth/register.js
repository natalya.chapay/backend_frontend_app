const { Conflict } = require('http-errors');
const bcrypt = require('bcryptjs');
const { User } = require('../../models');
const createError = require('../../helpers/createError');
const { registerSchema } = require('../../schemas');

const register = async (req, res) => {
  const { error } = registerSchema.validate(req.body);
  if (error) {
    throw createError(400, error.message);
  }
  const { username, password, token } = req.body;
  const user = await User.findOne({ username });
  if (user) {
    throw new Conflict('Username in use');
  }
  const hashPassword = bcrypt.hashSync(password, bcrypt.genSaltSync(10));
  await User.create({ username, password: hashPassword, token });
  res.status(200).json({
    message: 'Success',
  });
};

module.exports = register;
