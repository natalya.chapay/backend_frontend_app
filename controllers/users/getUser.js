const { User } = require('../../models');

const getUser = async (req, res) => {
  const { user } = req;

  const { _id, username, createdDate } = await User.findOne(user);

  res.json({
    user: {
      _id,
      username,
      createdDate,
    },
  });
};

module.exports = getUser;
