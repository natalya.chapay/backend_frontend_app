const Joi = require('joi');

const add = Joi.object({
  text: Joi.string().required(),
  completed: Joi.boolean(),
});

module.exports = add;
