const { createError } = require('../../helpers');

const { Note } = require('../../models/note');

const removeNote = async (req, res) => {
  const { id } = req.params;
  const result = await Note.findByIdAndRemove(id);
  if (!result) {
    throw createError(404);
  }
  res.status(200).json({
    message: 'Success',
  });
};

module.exports = removeNote;
