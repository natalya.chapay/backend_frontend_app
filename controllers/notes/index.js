const listNotes = require('./listNotes');
const getById = require('./getById');
const addNote = require('./addNote');
const updateNote = require('./updateNote');
const removeNote = require('./removeNote');
const updateCompleted = require('./updateCompleted');

module.exports = {
  listNotes,
  getById,
  addNote,
  updateNote,
  removeNote,
  updateCompleted,
};
