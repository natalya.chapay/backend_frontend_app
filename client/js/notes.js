const logout = document.querySelector(".logout");
const notes = document.querySelector(".notes-list");
const addNoteBtn = document.querySelector(".add");
const addModal = document.querySelector(".backdrop-add");
const editModal = document.querySelector(".backdrop-edit");
const closeBtn = document.querySelector(".button-close");
const closeBtnEdit = document.querySelector(".button-close-edit");
const addForm = document.querySelector(".form-add");
const editForm = document.querySelector(".form-edit");
const notesLink = document.querySelector(".notes");
const userMenu = document.querySelector(".menu");
const main = document.querySelector(".main");
const pagination = document.querySelector(".pagination");


let username = "user";
let userId;

let deleteBtns;
let editBtns;
let completedBtns;
let id;
let prev;
let next;
let page = 1;
let totalPages;
let currentPage;

const token = localStorage.getItem("token");

editForm.addEventListener("submit", onEditFormSubmit);
addForm.addEventListener("submit", onAddFormSubmit);
closeBtn.addEventListener("click", onCloseBtnClick);
closeBtnEdit.addEventListener("click", onCloseEditBtnClick);
addNoteBtn.addEventListener("click", onAddBtnClick);
logout.addEventListener("click", onLogoutBtnClick);
userMenu.addEventListener("click", onUserMenuClick);

// ------------------Notelist------------------------------------

async function getNotes(page = 1) {
  const url = `http://localhost:8080/api/notes/?page=${page}&limit=5`;
  const fetchOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    },
  };

  const response = await fetch(url, fetchOptions);

  if (response.ok) {
    const result = await response.json();
    if (result.notes.length === 0) {
      alert("Your note list is empty");
    }
    renderNotes(result);
  }
}

function noteListTemplate(data) {
  return data
    .map(
      ({ _id, text }) => `<li id="${_id}" class="notes-item">
    <p>${text}</p>
    <div class="notes-option">
    <button id="completed${_id}" class="option-btn completed">✖ Completed</button>
    <button id="edit${_id}" class="option-btn edit">Edit</button>
    <button id="delete${_id}" class="option-btn delete">Delete</button>
    </div>
</li>`
    )
    .join("");
}

function renderNotes({notes: data, count}) {
  totalPages = count;
  notes.innerHTML = noteListTemplate(data);
  if (count > 0) {
    pagination.innerHTML = `<div class="pagination-list">
    <button class="pagination-btn prev">prev</button>
    <span type="button" class="pagination-page">${page}</span>
    <button type="button" class="pagination-btn next">
      next
    </button>
  </div>`
  prev = document.querySelector('.prev');
  next = document.querySelector('.next');
  currentPage = document.querySelector('.pagination-page');
  prev.addEventListener('click', onPrevBtnClick);
  next.addEventListener('click', onNextBtnClick);
  if (page === 1) {
    prev.disabled = true;
  }
  
  if (page === Math.ceil(totalPages / 5)) {
    next.disabled = true;
  }
  }
  data.forEach(({ _id, completed }) => {
    if (completed) {
      document.getElementById(_id).children[1].children[0].textContent =
        "✔ Completed";
    }
    completedBtns = document.querySelectorAll(".completed");
    completedBtns.forEach((btn) =>
      btn.addEventListener("click", onCompletedBtnClick)
    );
    deleteBtns = document.querySelectorAll(".delete");
    deleteBtns.forEach((btn) =>
      btn.addEventListener("click", onDeleteBtnClick)
    );
    editBtns = document.querySelectorAll(".edit");
    editBtns.forEach((btn) => btn.addEventListener("click", onEditBtnClick));
  });
}

getNotes();

// -----------------------Navigation--------------------------------

function onAddBtnClick() {
  addModal.classList.toggle("is-hidden");
}

function onCloseBtnClick() {
  addModal.classList.toggle("is-hidden");
}

function onCloseEditBtnClick() {
  editModal.classList.toggle("is-hidden");
}

function onLogoutBtnClick() {
  localStorage.removeItem("token");
}

function onCompletedBtnClick(event) {
  const id = event.target.id.slice(9);
  changeCompleted(id);
  getNotes();
}

// ------------------Change completed note status------------------

function changeCompleted(id) {
  return fetch(`http://localhost:8080/api/notes/${id}`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    },
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error();
    })
    .catch(() => alert("Note not found"));
}

// -----------------Delete note---------------------

async function onDeleteBtnClick(event) {
  id = event.target.id.slice(6);
  deleteNote(id);
  await getNotes();
}

function deleteNote(id) {
  return fetch(`http://localhost:8080/api/notes/${id}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    },
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error();
    })
    .catch(() => alert("Note not found"));
}

// ----------------------Edit note-------------------------

function onEditBtnClick(event) {
  event.preventDefault();
  id = event.target.id.slice(4);
  editModal.classList.toggle("is-hidden");
}

function editNote(id, formData) {
  return fetch(`http://localhost:8080/api/notes/${id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: formData,
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error();
    })
    .catch(() => alert("Note not found"));
}

function onEditFormSubmit(event) {
  event.preventDefault();
  const form = event.currentTarget;
  const formData = new FormData(form);
  const dataObject = Object.fromEntries(formData.entries());
  const formDataString = JSON.stringify(dataObject);
  editNote(id, formDataString);
  editModal.classList.toggle("is-hidden");
  getNotes();
}

// --------------------Add note-------------------------

function addNote(formData) {
  return fetch(`http://localhost:8080/api/notes/`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: formData,
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error();
    })
    .catch(() => alert("Something went wrong"));
}

function onAddFormSubmit(event) {
  event.preventDefault();
  const form = event.currentTarget;
  const formData = new FormData(form);
  const dataObject = Object.fromEntries(formData.entries());
  const formDataString = JSON.stringify(dataObject);
  addNote(formDataString);
  form.reset();
  addModal.classList.toggle("is-hidden");
  getNotes();
}

// -----------------------------------------------------------
// ---------------------User profile--------------------------
// -----------------------------------------------------------

async function onUserMenuClick() {
  const userData = await getUser();
  username = userData.username;
  userId = userData.userId;
  userMenu.classList.add("current");
  notesLink.classList.remove("current");
  notes.innerHTML = "";
  pagination.innerHTML = "";
  main.innerHTML = `<p class="main-title">Hello, ${username}!</p>
    <div class="profile-form">
      <form class="profile-form-pasword">
        <label for="old-passwod" name="oldPassword" class="form-label user"
          >Old password
          <input id="old-passwod" name="oldPassword" class="form-input user" placeholder="old password" />
        </label>
        <label for="new-passwod" name="newPassword" class="form-label user"
          >New password
          <input id="new-passwod" name="newPassword" class="form-input user" placeholder="new password" />
        </label>
        <button type="submit" class="form-btn">Save</button>
      </form>
      <button type="button" class="form-delete-btn user">Delete user</button>
    </div>`;
  const changePaswordBtn = document.querySelector(".profile-form-pasword");
  changePaswordBtn.addEventListener("submit", onChangePassword);
  const deleteUserBtn = document.querySelector(".form-delete-btn");
  deleteUserBtn.addEventListener("click", ondeleteUserBtn);
}

function getUser() {
  return fetch("http://localhost:8080/api/users/me", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    },
  })
    .then((response) => {
      if (response.ok) {
        return response.json().then((data) => {
          username = data.user.username;
          userId = data.user._id;
          return { userId, username };
        });
      }
      throw new Error();
    })
    .catch(() => alert("Not authorized"));
}

// --------------------Change user password---------------------

function onChangePassword(event) {
  event.preventDefault();
  const form = event.currentTarget;
  const formData = new FormData(form);
  const dataObject = Object.fromEntries(formData.entries());
  const formDataString = JSON.stringify(dataObject);
  changePasword(formDataString);
  form.reset();
  alert('Password changed successfully')
}

function changePasword(formData) {
  return fetch(`http://localhost:8080/api/users/me`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: formData,
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error();
    })
    .catch(() => alert("Note not found"));
}

// -----------------------Delete user---------------------------

async function ondeleteUserBtn(event) {
  event.preventDefault();
  await deleteUser();
  onLogoutBtnClick();
  alert('User deleted successfully')
  main.innerHTML = '<p class="main-title">Please, login</p>';
}

function deleteUser() {
  return fetch(`http://localhost:8080/api/users/me`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    },
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error();
    })
    .catch(() => alert("Note not found"));
}

// ------------------Pagination--------------------------------

function onNextBtnClick() {
  if (page === Math.ceil(totalPages / 5)) {
    return;
  }
  page++;
  currentPage.textContent = page;
  getNotes(page);
}

function onPrevBtnClick() {
  if (page === 1) {
    return;
  }
  page--;
  currentPage.textContent = page;
  getNotes(page);
}