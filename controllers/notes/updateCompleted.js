const { createError } = require('../../helpers');

const { Note } = require('../../models/note');

const updateNote = async (req, res) => {
  const { id } = req.params;
  const note = await Note.findById(id);
  const completed = { completed: !note.completed };
  const result = await Note.findByIdAndUpdate(id, completed, { new: true });
  if (!result) {
    throw createError(404);
  }
  res.status(200).json({
    message: 'Success',
  });
};

module.exports = updateNote;
