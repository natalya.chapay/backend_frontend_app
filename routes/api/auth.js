const express = require('express');

const auth = require('../../controllers/auth');

const { ctrlWrapper } = require('../../helpers');
const { validation } = require('../../middlewares');

const { registerSchema, loginSchema } = require('../../schemas');

const router = express.Router();

router.post('/register', validation(registerSchema), ctrlWrapper(auth.register));

router.post('/login', validation(loginSchema), ctrlWrapper(auth.login));

module.exports = router;
