const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { createError } = require('../../helpers');

const { User } = require('../../models');

const { SECRET_KEY } = process.env;
const { loginSchema } = require('../../schemas');

const login = async (req, res) => {
  const { error } = loginSchema.validate(req.body);
  if (error) {
    throw createError(400, error.message);
  }
  const { username, password } = req.body;
  const user = await User.findOne({ username });
  if (!user || !bcrypt.compareSync(password, user.password)) {
    throw createError(400, 'Username or password is wrong');
  }
  const payload = {
    id: user._id,
  };
  const jwtToken = jwt.sign(payload, SECRET_KEY);

  res.status(200).json({
    message: 'Success',
    jwt_token: jwtToken,
  });
};

module.exports = login;
