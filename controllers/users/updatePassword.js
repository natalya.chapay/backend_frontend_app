const bcrypt = require('bcryptjs');
const { User } = require('../../models');
const { createError } = require('../../helpers');
const { patchSchema } = require('../../schemas');

const updatePassword = async (req, res) => {
  const { error } = patchSchema.validate(req.body);
  if (error) {
    throw createError(400, error.message);
  }
  const { _id, username } = req.user;
  const { oldPassword, newPassword } = req.body;
  const user = await User.findOne({ username });

  if (!bcrypt.compareSync(oldPassword, user.password)) {
    throw createError(400, 'Wrong old password');
  }

  const hashPassword = bcrypt.hashSync(newPassword, bcrypt.genSaltSync(10));

  const result = await User.findByIdAndUpdate(_id, { password: hashPassword });
  if (!result) {
    throw createError(404);
  }
  res.status(200).json({
    message: 'Success',
  });
};

module.exports = updatePassword;
