const notes = require('./notes');
const users = require('./users');
const auth = require('./auth');

module.exports = {
  notes,
  users,
  auth,
};
