const Joi = require('joi');

const registerSchema = Joi.object({
  username: Joi.string().required(),
  password: Joi.string().min(4).required(),
});

module.exports = registerSchema;
