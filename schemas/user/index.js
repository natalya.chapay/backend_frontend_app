const registerSchema = require('./registerSchema');
const loginSchema = require('./loginSchema');
const patchSchema = require('./patchSchema');

module.exports = {
  registerSchema,
  loginSchema,
  patchSchema,
};
